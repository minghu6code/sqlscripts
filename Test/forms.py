
from django import forms

class addStuForm(forms.Form):
       
    Sno=forms.SlugField(initial='Sno',label='学号')
    Sname=forms.CharField(initial='Sname',label='姓名')
    Ssex=forms.CharField(initial='Ssex',label='性别')
    Clno=forms.SlugField(initial='Clno',label='班级')
    Sbirth=forms.DateField(initial=None,label='出生日期',required=False)
    Sentrance=forms.DateField(initial=None,label='入学日期',required=False)
    
class searchStuForm(forms.Form):
    
    Sno=forms.SlugField(initial='',required=False,label='学号')
    Sname=forms.CharField(initial='',required=False,label='姓名')
    Majorno=forms.SlugField(initial='',required=False,label='专业号')
    

class addGradeForm(forms.Form):
    
    Sno=forms.SlugField(initial='',required=True,label='学号')
    Cno=forms.SlugField(initial='',required=True,label='课程号')
    Grade=forms.IntegerField(required=True,label='成绩')
    Pass=forms.BooleanField(initial=True,required=False,label='通过')
    
    
class addGradeForm_1(forms.Form):
    
    Sno=forms.SlugField(initial='',required=True,label='学号')
    Cno=forms.SlugField(initial='',required=True,label='课程号')
    Grade=forms.IntegerField(required=True,label='成绩')
    Pass=forms.BooleanField(initial=True,required=False,label='通过')
    confirm=forms.BooleanField(initial=False,required=False,label='确定狠心挂掉(前提是选择不通过)')
 
    
class searchStu_TeacherForm(forms.Form):
    
    Sno=forms.SlugField(initial='',required=False,label='学号')
    Sname=forms.CharField(initial='',required=False,label='姓名')
    Cno=forms.SlugField(initial='',required=False,label='课程号')
    Cname=forms.CharField(initial='',required=False,label='课程名')

class Stu_WarningForm(forms.Form):
    deadline_req=forms.IntegerField(initial=15,required=False,label='必修课死亡线')
    deadline_elec=forms.IntegerField(initial=20,required=False,label='选修课死亡线')
    AlertValue=forms.IntegerField(initial=3,required=False,label='警戒差')
    
    
    
    
    
    
    
    