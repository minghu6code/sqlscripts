#coding:utf-8

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

def index(request):
    return HttpResponse("你好，Django")

def home(request):
    
    msg1='www.zhegeyuming.shihubiande.com'
    #msg2=555
    return render(request,'home.html',{'msg1':msg1})

def cleanUnicode(Str):
    '''
    remove extra space and all the quotes
    '''
    import re
    Str=Str.strip()
    Str=''.join(re.split('[\'|\"]',Str))
    return Str


'''
deal with addGrade
'''
def checkEnum(Str,tp):
    '''
    check the enum type for database,
    Str->checked character String
    tp->enum range(tuple set etc compatible \"in\")
    '''
    if Str in tp:
        return Str
    else:
        raise(TypeError)

def addStuForm_deal(request):
    '''
    retrive url and deal with the form of (.forms.addStuForm and addStu.html)
    '''
    from .forms import addStuForm
    from .querydb import XdEduDb
    
    
    if request.method=='POST':
        form=addStuForm(request.POST)
        
        if form.is_valid():
            '''
            deal with the format of the data
            '''
            #StuInfo_key=['Sno','Sname','Ssex','Clno','Sbirth','Sentrance']
            StuInfo=dict.fromkeys(form.cleaned_data.keys())
            Ssex_pattern=('男','女','shemale')
            ok='Query OK!'
            ExtraInfo={ok}
            for key in StuInfo.keys():
                StuInfo[key]=form.cleaned_data[key]
                if key not in ('Sbirth','Sentrance'):
                    StuInfo[key]=cleanUnicode(StuInfo[key])
                if key=='Ssex':
                    try:
                        StuInfo[key]=checkEnum(StuInfo[key],Ssex_pattern)
                    except TypeError:
                        ExtraInfo.discard(ok)
                        ExtraInfo.add('性别类型错误，男或女')
            
            db=XdEduDb(passwd='19678zy')
            '''
            test if query ok 
            '''
           
            try:
                db.instStu(**StuInfo)
            except Exception as e:
                ExtraInfo.discard(ok)
                ExtraInfo.add(e.args)
            finally:
                db.close()
                
            form=addStuForm(initial=StuInfo)
            return render(request,'addStu.html',{'form':form,'ExtraInfo':ExtraInfo})
        else:
            return HttpResponse('form\'s value Error')
    else:
        form=addStuForm()
        return render(request,'addStu.html',{'form':form})
    
def searchStuForm_deal(request):
    '''
    retrive url and deal with the form of (.forms.searechStuForm and  searechStu.html)
    '''
    from .forms import searchStuForm
    from .querydb import XdEduDb
    
    if request.method=='POST':
        
        form=searchStuForm(request.POST)
        if form.is_valid():
            '''
            deal with the format of the data
            '''
            BinList=[v=='' for v in form.cleaned_data.values()]
            if False in BinList: #at least one item not None
                SearchInfo={}
                for key in form.cleaned_data.keys():
                    if form.cleaned_data[key]!='':
                        SearchInfo[key]=form.cleaned_data[key]
                        SearchInfo[key]=cleanUnicode(SearchInfo[key])
                
                db=XdEduDb(passwd='19678zy')
                '''
                test if query ok 
                '''
                ExtraInfo=('Query OK!')
                QueryResult=({'并没有'})
                
                cache=list()
                
                
                split_p=['- - - - - - - - - - - - - - - - - - - - - - - \
                    - - - - - - - - - - - - - - - - - - - - - - -']
            
                if request.POST.__contains__('searchGradeInfo'):
                    QueryResult=db.searchStu_grade(**SearchInfo)
                    Stu_p=['学号','姓名','班级','专业名']
                    CGrade_p=['课程号','课程名','性质','学分','开设学期','成绩','补考次数','通过']
                    GradeAve_p=['全部课程加权平均成绩','必修课加权平均成绩']
                    SnoSet=set()
                    for item in QueryResult:
                        #item[0]:Sno
                        if item[0] not in SnoSet:
                            SnoSet.add(item[0])

                            cache.append(split_p)
                            Stu=[item[0],item[1],item[2],item[4]]
                            cache.append(Stu_p)
                            cache.append(Stu)

                            GradeAve=[item[17],item[18]]
                            cache.append(GradeAve_p)
                            cache.append(GradeAve)

                        CGrade=[item[5],item[6],item[7],item[8],item[9],item[11],item[12],item[13]]
                        cache.append('\n')
                        cache.append(CGrade_p)
                        cache.append(CGrade)


                    QueryResult=cache
                    
                elif request.POST.__contains__('searchStuInfo'):
                    QueryResult=db.searchStu_all(**SearchInfo)
                    Stu_p=['学号','姓名','班级','专业号','专业名','性别','入学日期','出生日期']
                    
                    for item in QueryResult:
                        cache.append(split_p)
                        
                        cache.append(Stu_p)
                        Stu=[item[0],item[1],item[2],item[6],item[7],item[5],item[3],item[4]]
                        cache.append(Stu)
                        
                    QueryResult=cache
                else:
                    pass
                
            
                form=searchStuForm(initial=SearchInfo)
                return render(request,'searchStu.html',{'form':form,'ExtraInfo':ExtraInfo,'SearchInfo':SearchInfo,'QueryResult':QueryResult})
             
            else:
                form=searchStuForm()
                return render(request,'searchStu.html',{'form':form})
        else:
            return HttpResponse('form\'s value Error') 
                
    else:
        form=searchStuForm()
        return render(request,'searchStu.html',{'form':form}) 
    
def addGradeForm_deal(request):
    '''
    retrive url and deal with the form of (.forms.addGradeForm and addGrade.html)
    '''
    from .forms import addGradeForm
    from .forms import addGradeForm_1
    from .querydb import XdEduDb
    from django import forms
   
    ExtraInfo=set()
    ok='Query OK!'
    confirm=False
    ExtraInfo.add(ok)
    if request.method=='POST':
        form=addGradeForm_1(request.POST)
        
        if form.is_valid():
            '''
            deal with the format of the data
            '''
            db=XdEduDb(passwd='19678zy')
            GradeInfo=dict()
            for key in form.cleaned_data:
                GradeInfo[key]=form.cleaned_data[key]
                if key not in ('Grade','Pass','confirm'):
                    GradeInfo[key]=cleanUnicode(GradeInfo[key])
                
            if GradeInfo['Pass']==True:
                confirm=True
                GradeInfo['Pass']='Yes'
            else:#False
                try :
                    confirm=form.cleaned_data['confirm']
                except KeyError:
                    confirm=False
                    
                QueryResult=db.searchStu_all(Sno=GradeInfo['Sno'])
                
                #addGradeForm_1.confirm=forms.BooleanField(initial=False,required=False,label='确定狠心挂掉')
                form=addGradeForm_1(initial=form.cleaned_data)
                
                form.confirm=forms.BooleanField(initial=False,required=False,label='确定狠心挂掉')
                    
                if confirm==False:
                    try:
                        Sname=QueryResult[0][1]
                        Ssex=QueryResult[0][3]

                        if Ssex=='男':
                            Ssex='哥们'
                        elif Ssex=='女':
                            Ssex='妹子'
                        else :
                            Ssex='外星人'

                        mayMsg=('报告老师，%s 这个 %s 数据库酱还是了解的，平日里勤奋努力，刻苦学习'
                            '然而这次考试前大病一场，脑癌晚期，成绩不是很理想，还请老师放他一条生路'
                            %(Sname,Ssex))
                        ExtraInfo.discard(ok)
                        ExtraInfo.add(mayMsg)
                        
                        return render(request,'addGrade.html',{'form':form,'ExtraInfo':ExtraInfo})
                        
                    except IndexError as e:
                        print(e,len(QueryResult))
                        pass
                    
                    #print('Failed-False')
                    print(ExtraInfo)
                    return render(request,'addGrade.html',{'form':form,'ExtraInfo':ExtraInfo})
                
                else:#confirm == True
                    GradeInfo['Pass']='No'
                    try:
                        GradeInfo.pop('confirm',None)
                        db.instGrade(**GradeInfo)
                    except Exception as e:
                        ExtraInfo.discard(ok)
                        ExtraInfo.add(e.args)
                    finally:
                        db.close()
                   
                    form=addGradeForm(initial=GradeInfo)
                    return render(request,'addGrade.html',{'form':form,'ExtraInfo':ExtraInfo})
            
            
            '''
            test if query ok 
            '''
            if  GradeInfo['Pass']=='Yes' or confirm==True:
                try:
                    GradeInfo.pop('confirm',None)
                    db.instGrade(**GradeInfo)
                except Exception as e:
                    ExtraInfo.discard(ok)
                    ExtraInfo.add(e.args)
                finally:
                    db.close()
            else:#failed and not sure
                ExtraInfo.discard(ok)
                ExtraInfo.add('人间何处不温暖')
                
            form=addGradeForm(initial=form.cleaned_data)
            return render(request,'addGrade.html',{'form':form,'ExtraInfo':ExtraInfo})
        else:
            return HttpResponse('form\'s value Error')
    else:
        form=addGradeForm()
        return render(request,'addGrade.html',{'form':form})
    
    
    
def searchStu_TeacherForm_deal(request):
    '''
    retrive url and deal with the form of (.forms.searechStu_TeacherForm and  searchStu_Teacher.html)
    '''
    from .forms import searchStu_TeacherForm
    from .querydb import XdEduDb
    
    if request.method=='POST':
        form=searchStu_TeacherForm(request.POST)
        if form.is_valid():
            '''
            deal with the format of the data
            '''
            BinList=[v=='' for v in form.cleaned_data.values()]
            if False in BinList: #at least one item not None
                SearchInfo={}
                for key in form.cleaned_data.keys():
                    if form.cleaned_data[key]!='':
                        SearchInfo[key]=form.cleaned_data[key]
                        SearchInfo[key]=cleanUnicode(SearchInfo[key])
                
                db=XdEduDb(passwd='19678zy')
                '''
                test if query ok 
                '''
                ExtraInfo=('Query OK!')
                QueryResult=({'并没有'})
                
                try:
                    #SearchInfo['table']='Student_Major'
                    QueryResult=db.searchStu_Teacher(**SearchInfo)
                except Exception as e:
                    ExtraInfo=('Error:',)+e.args
                else:
                    cache=list()
                    Stu_p=['学号','姓名']
                    Teacher_p=['课程号','课程名','教师姓名','教师编号']
                    
                    split_p=['- - - - - - - - - - - - - - - - - - - - - - - \
                    - - - - - - - - - - - - - - - - - - - - - - -']
                    
                    Stuno=set()
                    for item in QueryResult:
                        if item[0] not in Stuno:
                            Stuno.add(item[0])
                            
                            cache.append(split_p)
                            Stu=[item[0],item[1]]
                            cache.append(Stu_p)
                            cache.append(Stu)
                            cache.append('\n')
                                                
                        
                        Teacher=[item[2],item[3],item[5],item[4]]
                        cache.append(Teacher_p)
                        cache.append(Teacher)

                    QueryResult=cache     
            
                form=searchStu_TeacherForm(initial=SearchInfo)
                return render(request,'searchStu_Teacher.html',
                              {'form':form,'ExtraInfo':ExtraInfo,'SearchInfo':SearchInfo,'QueryResult':QueryResult})
             
            else:
                form=searchStu_TeacherForm()
                return render(request,'searchStu_Teacher.html',{'form':form})
        else:
            return HttpResponse('form\'s value Error') 
                
    else:
        form=searchStu_TeacherForm()
        return render(request,'searchStu_Teacher.html',{'form':form})    
    
def Stu_WarningForm_deal(request):
    '''
    retrive url and deal with the form of (.forms.Stu_WarningForm and  Stu_Warning.html)
    '''
    from .forms import Stu_WarningForm
    from .querydb import XdEduDb
    
    if request.method=='POST':
        
        form=Stu_WarningForm(request.POST)
        if form.is_valid():
            '''
            deal with the format of the data
            '''
            BinList=[v=='' for v in form.cleaned_data.values()]
            if False in BinList: #at least one item not None
                SearchInfo={}
                for key in form.cleaned_data.keys():
                    if form.cleaned_data[key]!='':
                        SearchInfo[key]=form.cleaned_data[key]
                        
                
                db=XdEduDb(passwd='19678zy')
                '''
                test if query ok 
                '''
                ExtraInfo=('Query OK!')
                QueryResult=({'并没有'})
                
                try:
                    #SearchInfo['table']='Student_Major'
                    QueryResult=db.Stu_Warning(**SearchInfo)
                except Exception as e:
                    ExtraInfo=('Error:',)+e.args
                else:
                    cache=list()
                    Stu_p=['学号','姓名','必修课不及格的学分','选修课不及格的学分']
                    
                    split_p=['- - - - - - - - - - - - - - - - - - - - - - - \
                    - - - - - - - - - - - - - - - - - - - - - - -']
                    
                    for item in QueryResult:
                
                        cache.append(split_p)
                        Stu=[item[0],item[1],item[2],item[3]]
                        cache.append(Stu_p)
                        cache.append(Stu)
                        cache.append('\n')
                                                
                    QueryResult=cache     
            
                form=Stu_WarningForm(initial=SearchInfo)
                return render(request,'Stu_Warning.html',
                              {'form':form,'ExtraInfo':ExtraInfo,'SearchInfo':SearchInfo,'QueryResult':QueryResult})
             
            else:
                form=Stu_WarningForm()
                return render(request,'Stu_Warning.html',{'form':form})
        else:
            return HttpResponse('form\'s value Error') 
                
    else:
        form=Stu_WarningForm()
        return render(request,'Stu_Warning.html',{'form':form})    
    
    
    
    
    
    