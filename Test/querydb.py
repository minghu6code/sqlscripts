#-*- Coding:utf-8 -*-
#encoding=utf-8
# python 3.4+pymysql+mysql5.5

#import pymysql

import mysql.connector as pymysql
from datetime import date
from decimal import Decimal
class XdEduDb:
    def __init__(self,host='localhost',port=3306,user='root',passwd='123456',\
                 db='XdEduDb1',charset='utf8'):
        
        self.conn,self.cur=self.connect(host,port,user,passwd,\
                                        db,charset)

    def connect(self,host,port,user,passwd,db,charset): 
        try:
            conn=pymysql.connect(host=host,port=port,user=user,passwd=passwd,\
                                        db=db,charset=charset)
            cur=conn.cursor()
            cur.execute('use {0:s}'.format(db))
            cur.execute(' SET NAMES \'utf8\' ')
        except pymysql.Error:
            print('Error')
        else:
            print('connect ok\nuse {0:s}'.format(db))
            return conn,cur

    def _Fetch_print(self):
        '''
        fetch result after query
        '''

        
        msg=self.cur.fetchall()
        self.conn.get_warnings=True
        err=self.cur.fetchwarnings()
        PlainMsg=list()
        PlainErr=list()
        for s in msg:
            line=[]
            for ss in s:
                try:
                    cache=ss.decode()
                except:
                    if type(ss)==date:
                        cache=ss.isoformat()
                    elif type(ss)in (float,int,Decimal):
                        cache=str(ss)
                    else:
                        cache=repr(ss)
                finally:
                    print(cache,end=' ')
                    line.append(cache)
                    
            print('')
            PlainMsg.append(line)
        '''
        if err!=None:    
            for s in err:
                line=[]
                for ss in s:
                    cache=ss
                    print(cache,end='')
                    line.append(cache)

                print('')
                PlainErr.append(line)
        '''    
        return PlainMsg
        
    def echo(self,TbName):
        msg='select * from %s'%(TbName)
        self.cur.execute(msg)
        self._Fetch_print()

        
    def _selec_k(self,**kwargs):
        '''
        select * from tablename where key1=value1 and key2=value2 etc
        need security check for key 'table'
        op default '='
        '''
        
        BaseQuery='select * from '
        try:
            table=kwargs['table']
            BaseQuery+=table
            kwargs.pop('table')
        except KeyError as e:
            print('no key word:table ')
            return

        try:
            op=kwargs.pop('op')
            op=' '+op+' '
        except KeyError:
            op='='
            
        ReqQuery=()
        if len(kwargs)>0:
            BaseQuery+=' where 1'
            
            for key in kwargs:
                BaseQuery += ' and %s%s%s '%(key,op,repr(kwargs[key]))
            
        #print(BaseQuery)
        msg=list()
        try:
            self.cur.execute(BaseQuery)
            msg=self._Fetch_print()
            
        except Exception as e:
            print(e)
            raise
        finally:
            return msg
    
    def instStu(self,Sno,Sname,Clno,Ssex='shemale',Sbirth=None,Sentrance=None):
        
        StuAdd=('insert into Student(Sno,Sname,Clno,Ssex,Sbirth,Sentrance) '
                'values'
                '(%(Sno)s,%(Sname)s,%(Clno)s,%(Ssex)s,%(Sbirth)s,%(Sentrance)s) ')
        StuData={
            'Sno':Sno,
            'Sname':Sname,
            'Clno':Clno,
            'Ssex':Ssex,
            'Sbirth':Sbirth,
            'Sentrance':Sentrance
        }
        try:
            self.cur.execute(StuAdd,StuData)
        except Exception as e:
            print(e)
            raise
        
    def searchStu_grade(self,Sno='%',Sname='%',Majorno='%'):
        msg=list()
        try:

            msg=self._selec_k(table='Student_Course_Grade_All',Sno=Sno,\
                          Sname=Sname,Majorno=Majorno,op='like')
        except Exception as e:
            print(e)
            raise
        finally:
            return msg
    def searchStu_all(self,Sno='%',Sname='%',Majorno='%'):
        msg=list()
        try:

            msg=self._selec_k(table='Student_Major',Sno=Sno,\
                          Sname=Sname,Majorno=Majorno,op='like')
        except Exception as e:
            print(e)
            raise
        finally:
            return msg
    
    def searchStu_Teacher(self,Sno='%',Sname='%',Cno='%',Cname='%'):
        msg=list()
        try:

            msg=self._selec_k(table='Student_Teacher',Sno=Sno,\
                              Sname=Sname,Cno=Cno,\
                              Cname=Cname,op='like')
            
        except Exception as e:
            print(e)
            raise
        finally:
            return msg

    
    def instGrade(self,Sno,Cno,Grade,Pass='Yes'):
        '''
        you must decide that 'pass' manually
        '''
        GradeAdd=('insert into Grade(Sno,Cno,Grade,resists,Pass) '
                  'values(%(Sno)s,%(Cno)s,%(Grade)s,%(resists)s,%(Pass)s)')
        GradeData={
            'Sno':Sno,
            'Cno':Cno,
            'Grade':Grade,
            'resists':0,
            'Pass':Pass}
        try:
            self.cur.execute(GradeAdd,GradeData)
        except Exception as e:
            print(e)
            raise
    
    def Stu_Warning(self,deadline_req=15,deadline_elec=20,AlertValue=3):
        if deadline_req==None:
            deadline_req=127

        if deadline_elec==None:
            deadline_elec=127
            
        deadline_req-=AlertValue
        deadline_elec-=AlertValue
        msg=list()
        try:
            msg_req=self._selec_k(table='FailedCcredit_req',
                              FailedCcredit_req=deadline_req,
                              op='>=')
            msg_elec=self._selec_k(table='FailedCcredit_elec',
                              FailedCcredit_elec=deadline_elec,
                              op='>=')
            #self union
            for item in msg_req:
                Ccredit_elec=None
                size=len(msg_elec)
                for i in range(size):
                    #item[0]:Sno
                    if item[0] == msg_elec[i][0]:
                        Ccredit_elec=msg_elec[i][2]#FailedCcredit_elec
                        msg_elec.pop(i)
                        break
                item.append(Ccredit_elec)
                msg.append(item)
            for item in msg_elec:
                #isert site is in front of index id 
                item.insert(3,None)
                msg.append(item)

        except Exception as e:
            raise
        finally:
            return msg
    def delStu_Sno(self,Sno):
        sql_del='delete '+\
                 'from Student'+\
                 'where Sno={0:s}'.format(Sno)
        self.cur.execute(sql_del)
        
    def close(self):
        self.conn.commit()
        self.conn.close()
        print('Bye')
    
    def __del__(self):
        try:
            self.close()
        except:
            pass
    
if __name__=='__main__':
    db=XdEduDb(passwd='19678zy')
    #db.instStu('03020007','小厮',Clno='0302',Sbirth=date(1997,4,4))
    d1={'Sno':'03050003'}
    #msg=db.searchStu_all(**d1)
    #print(msg)
    #print(msg[0][1],msg[0][3])
    #msg=db.searchStu_Teacher(**d1)
    #print(msg)
    #db.echo('Student')
    'insert student'
    #db._selec_k(table='Student_Major',Clno='%',op='like')
    'insert grade'
    #err=db.instGrade('03020001','HA2113001','60',Pass='No')
    #print(err)
    'query course'
    #db._selec_k(table='Student_Course_Grade')
    'query ave grade'
    #db._selec_k(table='GradeAve_all',Sno='03050001')
    #db._selec_k(table='GradeAve_req',Sno='03050004')
    'query Student\'s Teacher '
    #db._selec_k(table='Stu_Teacher',Sno='03050004')
    'warning for failed student'
    msg=db.Stu_Warning()
    print(msg)
    db.close()
    
        
