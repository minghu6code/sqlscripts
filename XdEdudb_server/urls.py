"""XdEdudb_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url,patterns
from django.contrib import admin

urlpatterns = [
    url(r'^$', 'Test.views.home',name='home_url'),
    url(r'^addStuForm$', 'Test.views.addStuForm_deal',name='addStuForm_url'),
    url(r'^searchStuForm$', 'Test.views.searchStuForm_deal',name='searchStuForm_url'),
    url(r'^searchStu_TeacherForm$', 'Test.views.searchStu_TeacherForm_deal',name='searchStu_TeacherForm_url'),
    url(r'^Stu_WarningForm$', 'Test.views.Stu_WarningForm_deal',name='Stu_WarningForm_url'),
    url(r'^addGradeForm$', 'Test.views.addGradeForm_deal',name='addGradeForm_url'),
    url(r'^admin/',include(admin.site.urls)),
]
